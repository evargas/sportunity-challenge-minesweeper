import Minesweeper from '@/components/minesweeper'
import MainTemplate from '@/layouts/main'

const MinesweeperPage = () => {
  const config = {
    bombs: 5,
    rows: 5,
    columns: 10,
  }
  return (
    <MainTemplate>
      <h1 className="text-3xl font-bold underline">
        Buscaminas
      </h1>
      <Minesweeper
        bombs={config.bombs}
        rows={config.rows}
        columns={config.columns}
      />
    </MainTemplate>
  )
}

export default MinesweeperPage;