export const compareNumbers = (a, b) => {
  return a - b;
}

export const getRandomNumbers = (num, max) => {
  let numbers = [];

  while (numbers.length < num) {
    let numero = Math.floor(Math.random() * max) + 1;
    if (!numbers.includes(numero)) {
      numbers.push(numero);
    }
  }

  return numbers.sort(compareNumbers)
}

export const getMatrix = (rows, columns) => {
  let k = 1
  let matrix = []
  for (let i = 0; i < rows; i++) {
    matrix[i] = []
    for (let j = 0; j < columns; j++) {
      matrix[i][j] = k
      k++
    }
  }

  return matrix
}

export const getNeighbors = (matrix, row, column) => {
  let neighbors = [];

  // vecino superior izquierdo
  if (row > 0 && column > 0) {
    neighbors.push(matrix[row - 1][column - 1]);
  }

  // vecino superior
  if (row > 0) {
    neighbors.push(matrix[row - 1][column]);
  }

  // vecino superior derecho
  if (row > 0 && column < matrix[row].length - 1) {
    neighbors.push(matrix[row - 1][column + 1]);
  }

  // vecino izquierdo
  if (column > 0) {
    neighbors.push(matrix[row][column - 1]);
  }

  // vecino derecho
  if (column < matrix[row].length - 1) {
    neighbors.push(matrix[row][column + 1]);
  }

  // vecino inferior izquierdo
  if (row < matrix.length - 1 && column > 0) {
    neighbors.push(matrix[row + 1][column - 1]);
  }

  // vecino inferior
  if (row < matrix.length - 1) {
    neighbors.push(matrix[row + 1][column]);
  }

  // vecino inferior derecho
  if (row < matrix.length - 1 && column < matrix[row].length - 1) {
    neighbors.push(matrix[row + 1][column + 1]);
  }
  return neighbors
}

export const getNearBombs = (neighbors, bombs) => {
  let nearBombs = 0
  for (let i = 0; i < neighbors.length; i++) {
    if (!!bombs.find(b => b === neighbors[i])) {
      nearBombs++
    }
  }
  return nearBombs
}

export const verifyWinner = (array1, array2) => {
  if(!array1 || !array2){
    return false
  }

  let arrayForSort1 = [...array1]
  let arrayForSort2 = [...array2]

  arrayForSort1.sort();
  arrayForSort2.sort();

  if (arrayForSort1.length !== arrayForSort2.length) {
    return false;
  }

  for (let i = 0; i < arrayForSort1.length; i++) {
    if (arrayForSort1[i] !== arrayForSort2[i]) {
      return false;
    }
  }

  return true;
}
  