import EggIcon from '@mui/icons-material/Egg';
import CheckIcon from '@mui/icons-material/Check';
import CoronavirusIcon from '@mui/icons-material/Coronavirus';
import { styled } from '@mui/material/styles';
import { useSelector, useDispatch } from 'react-redux'
import { activateCell, startGame, setFlag } from '../../redux/reducers/minesweeper'
import { RootState } from '@/redux/store';
import FlagIcon from '@mui/icons-material/Flag';
import { IconButton } from '@mui/material';

const CustomSpan = styled('span')`
    width: 24px;
    border-radius: 100%;
    color: #888;
    font-size: 20px !important;
    font-family: 'Roboto' !important;
`

const Cell = ({ item }: any) => {
  
  const dispatch = useDispatch()
  const reveled = useSelector((state: RootState) => state.minesweeper.game.reveled)

  const handleClick = () => {
    if (item.isHidden) {
      if (reveled?.length === 0) {
        dispatch(startGame(1))
      }
      setTimeout(() => {
        dispatch(activateCell(item.index.toString()))
        if (item.hasBomb) {
          dispatch(startGame(2))
        }
      },200)
    }
  };

  const handleRightClick = (e: any) => {
    e.preventDefault()
    dispatch(setFlag(item.index.toString()))
  }

  return (
    <>
      {
        item ?
          <IconButton
            color="primary"
            aria-label="mine"
            onClick={() => handleClick()}
            onContextMenu={(e) => handleRightClick(e)}>
            {
              item ?
                item.isHidden ?
                  item.flag ?
                    <FlagIcon color="success" /> :
                    <EggIcon /> :
                  item.hasBomb ?
                    <CoronavirusIcon color="error" /> :
                    item.nearBombs !== 0 ?
                      <CustomSpan className="material-icons">{item.nearBombs}</CustomSpan> :
                      <CheckIcon />
                : null
            }
          </IconButton>
          : null
      }
    </>
  );
}

export default Cell
