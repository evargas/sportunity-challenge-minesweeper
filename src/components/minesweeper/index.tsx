import { useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { styled } from '@mui/material/styles';
import Header from '@/components/header';
import Board from '@/components/board'
import { getMatrix, getNearBombs, getNeighbors, getRandomNumbers, verifyWinner } from '@/helpers/functions';
import { setBombs, setGame, startGame } from '@/redux/reducers/minesweeper'
import { RootState } from '@/redux/store';

const Container = styled('div')`
    position: relative;
    z-index: 0;
    .game-modal{
      border: solid #612b08 2px;
      position: absolute;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      background-color: #c94b4b;
      z-index: 10;
      display: flex;
      justify-content: center;
      align-items: center;
      opacity: 0.9;
      h3{
        font-size: 30px;
        color: #612b08;
      }

      &--winer{
        background-color: #57cf73;
        border: solid #b21b1b 2px;
        h3{
          font-size: 30px;
          color: #b21b1b;
        }
      }
    }
`

export type MinesweeperType = {
    bombs: number;
    rows: number;
    columns: number;
}

const MinesWeeper = ({ bombs = 0, rows, columns }: MinesweeperType) => {
  const dispatch = useDispatch()
  const matrix: any = getMatrix(rows, columns)
  const map = useRef<any>({})
  
  const bombsState = useSelector((state: RootState) => state.minesweeper.game.bombs)
  const start = useSelector((state: RootState) => state.minesweeper.game.start)
  useEffect(() => {
    
    let bombsArr: number[] = []
    if (!(bombsState.length > 0)) {
      if (bombs > 0 || bombs < rows * columns) {
        bombsArr = getRandomNumbers(bombs, rows * columns)
      }
      dispatch(setBombs(bombsArr))
    }

    let k = 1
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < columns; j++) {
        let neighbors = getNeighbors(matrix, i, j)
        map.current = ({
          ...map.current,
          [k]: {
            neighbors,
            nearBombs: getNearBombs(neighbors, bombsState),
            hasBomb: !!bombsState.find(e => e === k),
            location: {
              x:i, y:j
            },
            index: k,
            isHidden: true,
            flag: false
          }
        })
        k++
      }
    }
    dispatch(setGame(map.current))
  })
  return (
    <>
      <Header />
      <Container>
        {
          start === 2 ? 
            <div className='game-modal'>
              <h3>Game Over</h3>
            </div> : null
        }
        {
          start === 4 ? 
            <div className='game-modal game-modal--winer'>
              <h3>Congrats!</h3>
            </div> : null
        }
        <Board rows={rows} columns={columns} />
      </Container>
    </>
  );
}

export default MinesWeeper;
