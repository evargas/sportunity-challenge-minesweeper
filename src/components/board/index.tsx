import { useEffect } from 'react';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import Cell from '@/components/cell';
import { verifyWinner } from '@/helpers/functions'
import { startGame } from '@/redux/reducers/minesweeper'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '@/redux/store'

type TableTypes = {
  rows: number;
  columns: number;
}

const TableContainer = styled('div')`
    border: solid red 2px;
    width: 416px;
    border-radius: 5px;
`

const Board = ({ rows, columns }: TableTypes) => {
  const dispatch = useDispatch()

  const items:any = useSelector((state: RootState) => state.minesweeper.game.items)
  const flags:number[] = useSelector((state: RootState) => state.minesweeper.game.flags)
  const bombs:number[] = useSelector((state: RootState) => state.minesweeper.game.bombs)

  useEffect(() => {
    if(flags.length > 0 && verifyWinner(flags, bombs)){
      dispatch(startGame(4))
    }
  }, [flags, bombs, dispatch])

  const renderCells = () => {
    const cells = [];
    let k = 1
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < columns; j++) {
        cells.push(<Cell key={`cell-${k}`} item={items[k]} />);
        k++
      }
    }
    return cells;
  };

  return (
    <TableContainer>
      <Grid justifyContent="center" container spacing={0} columns={columns}>{renderCells()}</Grid>
    </TableContainer>
  )
}

export default Board;