const Counter = ({ value, label = '' }: {  value: number | string | null, label: string }) => {

  return (
    <h4 className='counter'>
      {label ? <span>{`${label} `}</span> : ''}
      {value}
    </h4>
  );
}

export default Counter;