import { styled } from '@mui/material/styles';
import { useSelector } from 'react-redux'
import { RootState } from '@/redux/store';
import Counter from './counter';
import Start from './start';
import Clock from '../clock';

const HeaderContainer = styled('div')`
    display: flex;
    justify-content: space-between;
    align-items: center;
    border: solid #1976d2 2px;
    width: 416px;
    padding: 10px;
    border-radius: 5px;
    margin-bottom: 10px;
`

const Header = () => {
  
  const score = useSelector((state: RootState) => state.minesweeper.game.flags).length
  
  return (
    <HeaderContainer>
      <Counter value={score} label='Score:' />
      <Start />
      <Clock />
    </HeaderContainer>
  );
}

export default Header;