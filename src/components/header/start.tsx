import TagFacesIcon from '@mui/icons-material/TagFaces';
import SentimentVeryDissatisfiedIcon from '@mui/icons-material/SentimentVeryDissatisfied';
import SentimentSatisfiedAltIcon from '@mui/icons-material/SentimentSatisfiedAlt';
import { IconButton } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '@/redux/store';
import { startGame } from '../../redux/reducers/minesweeper'

const Start = () => {
  const start = useSelector((state: RootState) => state.minesweeper.game.start)
  const dispatch = useDispatch()

  const handleClick = () => {
    dispatch(startGame(0))
  };

  return (
    <div className='start-icon'>
      <IconButton color="primary" aria-label="start" onClick={() => handleClick()}>
        { 
          start === 2 ? 
            <SentimentVeryDissatisfiedIcon /> : 
              start === 1 ?
                <SentimentSatisfiedAltIcon /> :
                <TagFacesIcon />}
      </IconButton>
    </div>
  );
}

export default Start;