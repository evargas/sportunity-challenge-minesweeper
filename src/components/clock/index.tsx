import { useState, useRef, useEffect } from "react"
import { useSelector } from 'react-redux'
import { RootState } from '@/redux/store';

const Clock = () => {

    const start = useSelector((state: RootState) => state.minesweeper.game.start)
    const [seconds, setSeconds] = useState<number>(0)
    const intervalRef = useRef<number>()

    const handleStart = () => {
        intervalRef.current = window.setInterval(() => {
            setSeconds((seconds) => seconds + 1)
        }, 1000)
    }

    const handleReset = () => {
        setSeconds(0)
        window.clearInterval(intervalRef.current)
    }

    useEffect(() => {
        if(start === 1 && seconds === 0){
            handleStart()
        }
        if(start === 0 || start ===2){
            handleReset()
        }
    }, [start, seconds])

    return (
        <div className="flex flex-col items-center">
            <h4 className="text-6xl font-bold mb-8">Time: {seconds}s</h4>
        </div>
    )
}

export default Clock