import { configureStore } from '@reduxjs/toolkit'
import minesweeperReducer from '@/redux/reducers/minesweeper'

export const store = configureStore({
  reducer: {
    minesweeper: minesweeperReducer,
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch