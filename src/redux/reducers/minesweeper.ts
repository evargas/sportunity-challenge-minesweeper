import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { compareNumbers } from '@/helpers/functions';

type GameType = {
  items: Object;
  bombs: number[];
  reveled: number[];
  flags: number[];
  time: number;
  score: number;
  start: number;
}

export interface MinesweeperState {
  game: GameType
}

const initialState: MinesweeperState = {
  game: {
    items: {},
    bombs: [],
    reveled: [],
    flags: [],
    time: 0,
    score: 0,
    start: 0
  },
}

export const minesweeperSlice = createSlice({
  name: 'minesweeper',
  initialState,
  reducers: {
    setBombs: (state: any, action: PayloadAction<number[]>) => {
      state.game.bombs = action.payload
    },
    setGame: (state: any, action: PayloadAction<number[]>) => {
      state.game.items = action.payload
    },
    startGame: (state: any, action: PayloadAction<number>) => {
      if (action.payload === 0) {
        state.game = initialState.game
      } else {
        state.game.start = action.payload
      }
    },
    activateCell: (state: any, action: PayloadAction<string>) => {
      state.game.items[action.payload.toString()] = {
        ...state.game.items[action.payload.toString()],
        isHidden: false
      }
      state.game.reveled.push(parseInt(action.payload))
      if (state.game.items[action.payload.toString()].nearBombs === 0 &&
        !state.game.items[action.payload.toString()].hasBomb) {
        for (let index = 0; index < state.game.items[action.payload.toString()].neighbors.length; index++) {
          state.game.items[state.game.items[action.payload].neighbors[index].toString()].isHidden = false;
          state.game.reveled.push(parseInt(state.game.reveled.push(parseInt(action.payload))))
        }
      }
      const result = state.game.reveled.reduce((acc: number[],item: number)=>{
        if(!acc.includes(item)){
          acc.push(item);
        }
        return acc;
      },[])
      state.game.reveled = [...result.sort(compareNumbers)]
    },
    setFlag: (state: any, action: PayloadAction<string>) => {
      if(state.game.items[action.payload.toString()].flag){
        state.game.flags = [...state.game.flags.filter((f:any) => f.toString() !== action.payload)]
      }else{
        state.game.flags = [...state.game.flags, parseInt(action.payload)]
      }
      state.game.items[action.payload.toString()] = {
        ...state.game.items[action.payload.toString()],
        flag: !state.game.items[action.payload.toString()].flag
      }
      state.game.flags = [...state.game.flags.sort()]
    },
    setTime: (state: any, action: PayloadAction<number>) => {
      state.game.time = action.payload
    },
  },
})

export const {
  setBombs,
  setGame,
  startGame,
  activateCell,
  setFlag,
  setTime
} = minesweeperSlice.actions

export default minesweeperSlice.reducer