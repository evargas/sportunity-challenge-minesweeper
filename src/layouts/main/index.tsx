import styles from '@/styles/Home.module.css'
import { MainTemplateType } from './types';
import MainHead from './MainHead';
import MainHeader from './MainHeader';

const defaultHead = {
    title: 'React App title',
    description: 'React App description'
}

const MainTemplate = ({ children, head = defaultHead }: MainTemplateType) => (
    <>
        <MainHead head={head} />
        <MainHeader />

        <main className={styles.main}>
            {
                children
            }
        </main>
    </>
)

export default MainTemplate
