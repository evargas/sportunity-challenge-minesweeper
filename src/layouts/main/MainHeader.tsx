import Nav from '@/components/nav'

export default function MainHeader() {
    return (
        <header className="app-header">
            <Nav />
        </header>
    )
}
