import Head from 'next/head'
import { HeadType } from './types'

const MainHead = ({ head }: { head: HeadType }) => {
  /* eslint-disable */
  return (
    <Head>
      <title>{head.title}</title>
      <meta name="description" content={head.description} />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="icon" href="/babyoda.jpeg" />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
      />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />
    </Head>
    /* eslint-enable */
  )
}

export default MainHead
